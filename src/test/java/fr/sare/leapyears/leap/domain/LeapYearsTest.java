package fr.sare.leapyears.leap.domain;

import fr.sare.leapyears.UnitTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * @author sareaboudousamadou.
 */
@UnitTest
class LeapYearsTest {

  @Test
  void areLeapYearIfDivisibleBy400() {
    assertThat(new Year(2000).isLeapYear()).isTrue();
  }

  @Test
  void areNotLeapYearIfNotDivisibleBy400() {
    assertThat(new Year(401).isLeapYear()).isFalse();
  }

  @ParameterizedTest
  @ValueSource(ints = {1700, 1800, 1900, 2100})
  void areNotLeapYearIfBy100ButNotBy400(int value) {
    assertThat(new Year(value).isLeapYear()).isFalse();
  }

  @ParameterizedTest
  @ValueSource(ints = {2008, 2012, 2016})
  void areLeapYearIfDivisibleBy4ButNot100(int value) {
    assertThat(new Year(value).isLeapYear()).isTrue();
  }

  @ParameterizedTest
  @ValueSource(ints = {2017, 2018, 2019})
  void areNotLeapYearIfNotDivisibleBy4(int value) {
    assertThat(new Year(value).isLeapYear()).isFalse();
  }
}
