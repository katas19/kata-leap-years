package fr.sare.leapyears.leap.domain;

/**
 * @author sareaboudousamadou.
 */
public record Year(int year) {
  public boolean isLeapYear() {
    if (isDivisibleBy100() && isNotDivisibleBy400()) {
      return false;
    }

    return isDivisibleBy400() || (isDivisibleBy4() && isNotDivisibleBy100());
  }

  private boolean isDivisibleBy4() {
    return this.year % 4 == 0;
  }

  private boolean isDivisibleBy400() {
    return this.year % 400 == 0;
  }

  private boolean isDivisibleBy100() {
    return this.year % 100 == 0;
  }

  private boolean isNotDivisibleBy400() {
    return this.year % 400 != 0;
  }

  private boolean isNotDivisibleBy100() {
    return this.year % 100 != 0;
  }
}
